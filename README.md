# Installation #

**composer.json**
```json
"repositories": [
       {
           "type": "vcs",
           "url": "git@bitbucket.org:kosm_o/yii1-fontawesome.git"
       }
]
```

```bash 
composer require ok/yii1-fontawesome: "dev-master"
```

**main.php**
```php
'components' => array(
       'fontawesome' => array(
           'class' => 'vendor.ok.yii1-fontawesome.components.FontAwesome',
       ),
)
```