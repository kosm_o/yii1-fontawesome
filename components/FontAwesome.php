<?php

/**
 * Bootstrap application component.
 */
class FontAwesome extends CApplicationComponent {

    protected $_assetsUrl;

    /**
     * Initializes the component.
     */
    public function init() {
        if (Yii::getPathOfAlias('fontawesome') === false)
            Yii::setPathOfAlias('fontawesome', realpath(dirname(__FILE__) . '/..'));

        if (Yii::app() instanceof CConsoleApplication)
            return;
        Yii::app()->getClientScript()->registerCssFile($this->getAssetsUrl() . "/css/font-awesome.min.css");

        parent::init();
    }

    /**
     * Returns the URL to the published assets folder.
     * @return string the URL
     */
    public function getAssetsUrl() {
        if (isset($this->_assetsUrl))
            return $this->_assetsUrl;
        else {
            $assetsPath = Yii::getPathOfAlias('fontawesome.assets');
            $assetsUrl = Yii::app()->assetManager->publish($assetsPath, false, -1, YII_DEBUG);
            return $this->_assetsUrl = $assetsUrl;
        }
    }
}
